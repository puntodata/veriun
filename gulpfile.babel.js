'use strict';

import gulp from 'gulp'
import sass from 'gulp-sass'
import pug from 'gulp-pug'
import concat from 'gulp-concat'
import browserSync from 'browser-sync'
import plumber from 'gulp-plumber'
import notify from 'gulp-notify'
import imagemin from 'gulp-imagemin'
import rename from 'gulp-rename'
import autoprefixer from 'gulp-autoprefixer'
import uglify from 'gulp-uglify'
import csso from 'gulp-csso'
import cleanCSS from 'gulp-clean-css'
import uglifycss from 'gulp-uglifycss'
import ftp from 'vinyl-ftp'
import surge from 'gulp-surge'
import babel from 'gulp-babel'
import image from 'gulp-image'
import cssimport from 'gulp-cssimport'
import beautify from 'gulp-beautify'
import uncss from 'gulp-uncss'
import cssmin from 'gulp-cssnano'
import imageop from 'gulp-image-optimization'
import sourcemaps from 'gulp-sourcemaps'
import critical from 'critical'
var bless = require('gulp-bless');
var watch = require('gulp-watch');
var affected = require('gulp-jade-find-affected');
const pugInheritance = require('@unisharp/gulp-pug-inheritance');
var changed = require('gulp-changed');
import cache from 'gulp-cached'
import minifyjs from 'gulp-js-minify'

/*=========================================================================
   Minify JS
=========================================================================*/

gulp.task('minify-js', function(){
  gulp.src('src/Proyectos/OnlyFile/js/*.js')
    .pipe(minifyjs())
    .pipe(gulp.dest('dist/Proyectos/OnlyFile/js/'));
});



/*=========================================================================
   Minify CSS
=========================================================================*/

const Css = {
	css: {
		css: 'src/Proyectos/OnlyFile/css/*.css',
		_css: 'dist/Proyectos/OnlyFile/css/',
  },
}
gulp.task('css-compile', function () {
  gulp.src(Css.css.css)
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest(Css.css._css));
});
/*=========================================================================
   Image
=========================================================================*/
gulp.task('image-compile', function () {
  gulp.src('src/Proyectos/OnlyFile/img/*')
    .pipe(image())
    .pipe(gulp.dest('dist/Proyectos/OnlyFile/img/'));
});


/*=========================================================================
   Contenido Gulp
=========================================================================*/
/*
* 1-  gulp Proyectos
*/
var  proyect_name = 'veriun';
/*=========================================================================
   Lumi
=========================================================================*/


const nproyectos = {
  name: proyect_name
}
const Proyectos = {
  frameworks: {
    base: proyect_name,

  },
  styles: {
    scss: `src/Proyectos/${nproyectos.name}/scss/*.scss`,
    _scss: `src/Proyectos/${nproyectos.name}/scss/_includes/**/*.scss`
  },
  pug: {
    pug: `src/Proyectos/${nproyectos.name}/*.pug`,
    _pug: `src/Proyectos/${nproyectos.name}/_includes/*.pug`,
    ___pug: `src/Proyectos/${nproyectos.name}/_includes/**/*.pug`
  },
  js: {
    base: `src/Proyectos/${nproyectos.name}/js/`,
    js: `src/Proyectos/${nproyectos.name}/js/*.js`,
    jsmin: `dist/Proyectos/${nproyectos.name}/js/`,
  },
  font: {
    font: `src/Proyectos/${nproyectos.name}/fonts/**/*`,
  },
  dist: {
    files: `dist/Proyectos/${nproyectos.name}/`,
    filesCss: `dist/Proyectos/${nproyectos.name}/css/`,
    filesPug: `dist/Proyectos/${nproyectos.name}/`,
    css: 'css',
    components: 'components',
    images: `src/Proyectos/${nproyectos.name}/img/*`,
    imgmin: `dist/Proyectos/${nproyectos.name}/img/`,
    fontDist: `dist/Proyectos/${nproyectos.name}/fonts/`
  }

}






gulp.task('scripts', () => {
  return gulp.src(routes.scripts.js)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: Babel and Concat failed.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(concat('script.js'))
    .pipe(babel())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(routes.scripts.jsmin))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'JavaScript Minified and Concatenated!',
      message: 'your js files has been minified and concatenated.',
      sound: false
    }));
});
gulp.task('beautify', () => {
  return gulp.src(routes.scripts.js)
    .pipe(beautify({indentSize: 4}))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: Beautify failed.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    .pipe(gulp.dest(routes.scripts.base))
  .pipe(notify({
    title: 'JS Beautified!',
    message: 'beautify task completed.',
    sound: false
  }));
});
gulp.task('critical', () => {
  return gulp.src(routes.dist.filesHtml)
    .pipe(critical.stream({
      base: baseDirs.dist,
      inline: true,
      minify: true,
      html: routes.dist.filesHtml,
      css: routes.dist.filesCss,
      ignore: ['@font-face', /url\(/],
      width: 1300,
      height: 900
    }))
      .pipe(plumber({
        errorHandler: notify.onError({
          title: 'Error: Critical failed.',
          message: '<%= error.message %>',
          sound: false
        })
      }))
      .pipe(gulp.dest(dist.dist))
      .pipe(notify({
        title: 'Critical Path completed!',
        message: 'css critical path done!',
        sound: false
      }));
});
gulp.task('uncss', () => {
  return gulp.src(routes.dist.filesCss)
    .pipe(uncss({
      html: [routes.dist.filesHtml],
      ignore: ['*:*']
    }))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: UnCSS failed.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    .pipe(cssmin())
    .pipe(gulp.dest(routes.styles.css))
    .pipe(notify({
      title: 'Removed unusued CSS',
      message: 'UnCSS completed!',
      sound: false
    }));
});
gulp.task('images', () => {
  gulp.src(routes.dist.images)
    .pipe(imagemin())
    .pipe(gulp.dest(routes.dist.imgmin));
});




gulp.task('Proyectos_pug', () => {
  return gulp.src(Proyectos.pug.pug).pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error:Proyectos Compiling pug.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
  .pipe(cache('linting'))
  .pipe(pug({
      pretty: true
    }))
  .pipe(gulp.dest(Proyectos.dist.files))
  .pipe(browserSync.stream())
  .pipe(notify({
      title: 'Pug Compiled succesfully!',
      message: 'Pug task completed.',
      sound: false
    }));


})

gulp.task('Proyectos_scss', () => {
  return gulp.src(Proyectos.styles.scss)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: Compiling SCSS.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 99 versions'],
      cascade: false}))


    .pipe(cssimport({}))

    .pipe(rename('app.css'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(Proyectos.dist.filesCss))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'SCSS Compiled and Minified succesfully!',
      message: 'scss task completed.',
      sound: false
    }));
});
gulp.task('Proyectos_js', () => {
  return gulp.src(Proyectos.js.js)
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: Babel and Concat failed.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    //.pipe(sourcemaps.init())
    .pipe(concat('app-dev.js'))
    .pipe(babel())
    .pipe(uglify())
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest(Proyectos.js.jsmin))
    .pipe(browserSync.stream())
    .pipe(notify({
      title: 'JavaScript Minified and Concatenated!',
      message: 'your js files has been minified and concatenated.',
      sound: false
    }));
});
gulp.task('Proyectos_beautify', () => {
  return gulp.src(Proyectos.js.js)
    .pipe(beautify({indentSize: 2}))
    .pipe(plumber({
      errorHandler: notify.onError({
        title: 'Error: Beautify failed.',
        message: '<%= error.message %>',
        sound: false
      })
    }))
    .pipe(gulp.dest(Proyectos.js.jsmin))
  .pipe(notify({
    title: 'JS Beautified!',
    message: 'beautify task completed.',
    sound: false
  }));
});
gulp.task('Proyectos_images', () => {
  gulp.src(Proyectos.dist.images)
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 5,
      svgoPlugins: [{removeViewBox: true}]}))
    .pipe(gulp.dest(Proyectos.dist.imgmin));
});
gulp.task('Proyecto-font', function () {
    gulp.src(Proyectos.font.font)
        .pipe(gulp.dest(Proyectos.dist.fontDist));
});






gulp.task('Proy', () => {
  browserSync.init({
		server: `dist/Proyectos/${nproyectos.name}/`
	});

  gulp.watch([Proyectos.styles.scss, Proyectos.styles._scss], ['Proyectos_scss']);
  gulp.watch([Proyectos.pug.pug, Proyectos.pug._pug, Proyectos.pug.___pug], ['Proyectos_pug']);
  gulp.watch([Proyectos.js.js], ['Proyectos_js']);
  gulp.watch([Proyectos.font.font], ['Proyecto-font']);
  gulp.watch([Proyectos.dist.images], ['Proyectos_images']);
});


gulp.task('dev', ['Proyectos_pug', 'Proyectos_scss', 'Proyectos_js', 'Proyectos_images', 'Proyecto-font', 'Proy']);
